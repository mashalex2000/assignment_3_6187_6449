package javagame;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;


public class GameOverMenu extends BasicGameState{
	Image Background,GameOver,PlayAgain;
	Sound gameOver;
	
	
	public GameOverMenu(int state) {

	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{
		Background = new Image("res/background.png");
	}
	
	//Draw
	public void render(GameContainer gc, StateBasedGame sbg,Graphics g) throws SlickException {
		Background.draw(0,0);
		GameOver=new Image("res/GameOver.png");
		PlayAgain = new Image("res/PlayAgain.png");
		GameOver.draw(70,-70);
		PlayAgain.draw(110, 0, Color.red);
		gameOver= new Sound("res/Game-Over.wav");
		gameOver.play();
		}
	
	//animation movement
	public void update(GameContainer gc, StateBasedGame sbg,int delta) throws SlickException{
		int xpos = Mouse.getX();
		int ypos = Mouse.getY();
		Input input = gc.getInput();
		//classic
		if((xpos>148 && xpos<290) && (ypos>110 && ypos<185)) {
			if(input.isMouseButtonDown(0)) {
				sbg.enterState(4);
			}
		}
		if((xpos>342 && xpos<482) && (ypos>110 && ypos<185)) {
			if(input.isMouseButtonDown(0)) {
				sbg.enterState(0);
			}
		}
	}
	
	public int getID() {
		return 5;
	}


}