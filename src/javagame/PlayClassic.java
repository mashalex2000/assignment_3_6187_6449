package javagame;


import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.io.File;
import java.util.concurrent.ThreadLocalRandom;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Result;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.*;


public class PlayClassic extends BasicGameState{
	Sound Opening,Bombdisplay,Fruitdisplay,fruitcut,bombexplode,gameOver;
	public String mouse = "NO";
	int count =0;
	fruit[] fruits = new fruit[100];
	fruit fruit1,fruit2;
	Image Apple,Banana,Bomb,Peach,Background,Pineapple,Lives,Livesx;
	boolean quit = false;
	float grav = 0.98f, deltaTime = 100;
	int[] duration = {200,200};
	int randomPos = ThreadLocalRandom.current().nextInt(1,7);
	float posdiffx = 80, posdiffy = 370;
	float shiftX =posdiffx*randomPos;
	float shiftY =posdiffy;
	double velX =6;
	double velY =15;
	Vector2f velocity, resistance;
	boolean onscreen=true;
	int lives =3;
	int randomNum = ThreadLocalRandom.current().nextInt(0,7);
	int score =0,i=0,flag=0,gameoverflag=0,startover=0;
	float shifttestx = posdiffx*randomPos;
	float shifttesty= posdiffy;
	int randomNumX = ThreadLocalRandom.current().nextInt(0,2);
	int ypos;
	int xpos;
	Point prevPoint,endPoint,startPoint;
	MouseEvent e;
	Line2D line;
	
	public PlayClassic(int state) {
	}
	
	public Vector2f velocity() {
		return new Vector2f((float)fruits[i].velX,-(float)fruits[i].velY);
	} 
	
	public Vector2f resistance() {
		return new Vector2f(0,grav);
	} 
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{
		Opening=new Sound("res/OpeningTheme.wav");
		Opening.play();
		Background = new Image("res/background.png");
		Lives = new Image("res/Lives.png");
		Livesx= new Image ("res/LivesX.png");
		fruit1 = new fruit();
		fruit2 = new fruit();
		for(int x=0;x<100;x++) {
				fruits[x] = new fruit();
				randomNum=ThreadLocalRandom.current().nextInt(0,7);
				fruits[x].fruitImage= fruits[x].nextImagee(randomNum);
			}
		if(startover==1) {
			for(int x=0;x<100;x++) {
				fruits[x] = new fruit();
				randomNum=ThreadLocalRandom.current().nextInt(0,7);
				fruits[x].fruitImage= fruits[x].nextImagee(randomNum);
			}
		}
		line = new Line2D.Double(100, 100, 200, 200);
		prevPoint = new Point(0,0);
		startPoint = new Point(0,0);
		endPoint = new Point(0,0);
		}
	
	//Draw
	public void render(GameContainer gc, StateBasedGame sbg,Graphics g) throws SlickException {
		Background.draw(0,0);
		Lives.draw(510,1);
		Lives.draw(550,1);
		Lives.draw(590,1);
			if(lives<3) {
				Livesx.draw(506,-2);
				}
			if(lives<2) {
				Livesx.draw(546,-2);}
			if(lives<1) {
				Livesx.draw(586,-2);}
		if(lives<=0) {
			gameoverflag=1;
		}
		//if(fruits[i].sliced == false) {
			fruits[i].fruitImage.draw(fruits[i].shiftX,fruits[i].shiftY,50,50);
			fruits[i].fruitImage.setCenterOfRotation(25, 25);
			fruits[i].fruitImage.rotate(0.1f);
		//}else {
		//	fruit1.fruitImage.draw(fruits[i].shiftX,fruits[i].shiftY,50,50);
		//	fruit2.fruitImage.draw(fruits[i].shifttestx,fruits[i].shifttesty,50,50);
		//}
		if(count>=3) {
			//if(fruits[i+1].sliced == false) {
			fruits[i+1].fruitImage.draw(fruits[i+1].shiftX,fruits[i].shiftY,50,50);
			fruits[i+1].fruitImage.setCenterOfRotation(25, 25);
			fruits[i+1].fruitImage.rotate(0.1f);
			//}else {
			//	fruit1.fruitImage.draw(fruits[i+1].shiftX,fruits[i+1].shiftY,50,50);
			//	fruit2.fruitImage.draw(fruits[i+1].shiftX,fruits[i+1].shiftY,50,50);
			//}
		}
		if(count>=5) {
			//if(fruits[i+2].sliced == false) {
				fruits[i+2].fruitImage.draw(fruits[i+2].shiftX,fruits[i].shiftY,50,50);
				fruits[i+2].fruitImage.setCenterOfRotation(25, 25);
				fruits[i+2].fruitImage.rotate(0.1f);
			//}else {
				//fruit1.fruitImage.draw(fruits[i+2].shiftX,fruits[i+2].shiftY,50,50);
				//fruit2.fruitImage.draw(fruits[i+2].shiftX,fruits[i+2].shiftY,50,50);
			//}
		}
		//g.setColor(Color.red);
		//Graphics2D g2 = (Graphics2D) g;
		//g2.draw(line);
		g.setColor(Color.white);
		g.drawString("Score: "+score, 100, 30);
		if (gameoverflag==1) {
			lives=3;
			gameoverflag=0;
			count=0;
			posdiffx = 80;
			posdiffy = 370;
			 shiftX =posdiffx*randomPos;
			 shiftY =posdiffy;
			 velX =6;
			 velY =15;
			lives =3;
			 score =0;
			 i=0;
			 flag=0;
			 gameoverflag=0;
			 shifttestx = posdiffx*randomPos;
			 shifttesty= posdiffy;
			startover=1;
			sbg.enterState(5);
		}
		if(quit==true) {
			lives =3;
			sbg.enterState(6);
			quit=false;
			count=0;
			lives=3;
			posdiffx = 80;
			posdiffy = 370;
			 shiftX =posdiffx*randomPos;
			 shiftY =posdiffy;
			 velX =6;
			 velY =15;
			 score =0;
			 i=0;
			 flag=0;
			 gameoverflag=0;
			 shifttestx = posdiffx*randomPos;
			 shifttesty= posdiffy;
		}
	}
	
	
	//animation movement
	public void update(GameContainer gc, StateBasedGame sbg,int delta) throws SlickException{

		Input input = gc.getInput();
		xpos = Mouse.getX();
		ypos = Mouse.getY();
		System.out.println(lives);
		motion();
		if(fruits[i].shiftX+50>=xpos&&fruits[i].shiftX<=xpos&&((((fruits[i].shiftY-370)+40>=ypos)||(fruits[i].shiftY-370)+40>=-ypos))&&(((fruits[i].shiftY-370)<=ypos)||(fruits[i].shiftY-370)<=-ypos)) {
		isSliced(gc,input,fruits[i]);
		}
		if(count>=3) {
		if(fruits[i+1].shiftX+50>=xpos&&fruits[i+1].shiftX<=xpos&&((((fruits[i+1].shiftY-370)+40>=ypos)||(fruits[i+1].shiftY-370)+40>=-ypos))&&(((fruits[i+1].shiftY-370)<=ypos)||(fruits[i+1].shiftY-370)<=-ypos)) {
			isSliced(gc,input,fruits[i+1]);	
		}}
		if(count>=5) {
			if(fruits[i+2].shiftX+50>=xpos&&fruits[i+2].shiftX<=xpos&&((((fruits[i+2].shiftY-370)+40>=ypos)||(fruits[i+2].shiftY-370)+40>=-ypos))&&(((fruits[i+2].shiftY-370)<=ypos)||(fruits[i+2].shiftY-370)<=-ypos)) {
				isSliced(gc,input,fruits[i+2]);	
		}
		}
		
		if(fruits[i].shiftY > 421){
			i++;
			if(fruits[i-1].sliced!=true&&fruits[i-1].imageName!="res/Bomb.png") {
				lives--;
			}
			else
				fruits[i-1].sliced=false;
			if(count>2) {
			if(fruits[i].sliced!=true&&fruits[i].imageName!="res/Bomb.png") {
				lives--;
			}}
			else
				fruits[i].sliced=false;
			if(count>4) {
				if(fruits[i+1].sliced!=true&&fruits[i+1].imageName!="res/Bomb.png") {
					lives--;
				}
				else
					fruits[i+1].sliced=false;
			}
			NewFruit(gc, sbg);
			Fruitdisplay=new Sound("res/Throw-fruit.wav");
			Fruitdisplay.play();
			shuffle(fruits[i]);
			count++;
					}
				

/*		if(input.isKeyDown(Input.KEY_UP))
		{
			fruits[i].shiftY -= delta *.1f;
		}
		if(input.isKeyDown(Input.KEY_DOWN))
		{
			fruits[i].shiftY += delta *.1f;
		}
		if(input.isKeyDown(Input.KEY_LEFT))
		{
			fruits[i].shiftX -= delta *.1f;
		}
		if(input.isKeyDown(Input.KEY_RIGHT))
		{			fruits[i].shiftX += delta *.1f;	
		}*/
			
		if(input.isKeyDown(Input.KEY_ESCAPE))
			quit=true;
		}
	
	/*public void mousePressed() {
        prevPoint.setLocation(Mouse.getX(), Mouse.getY());;
        System.out.println("Prev Point=" + prevPoint.toString());
	    }
	
    public void mouseDragged(MouseEvent e) {
        int dx = 0;
        int dy = 0;

        dx = (int) (prevPoint.x - Mouse.getX());
        dy = (int) (prevPoint.y - Mouse.getY());

        Line2D shape = (Line2D) line;

        int x1 = (int) (shape.getX1() - dx);
        int y1 = (int) (shape.getY1() - dy);

        int x2 = (int) (shape.getX2() - dx);
        int y2 = (int) (shape.getY2() - dy);

        startPoint = new Point(x1, y1);
        endPoint = new Point(x2, y2);

        if (shape != null) {
            shape.setLine(startPoint, endPoint);
            prevPoint.setLocation(Mouse.getX(), Mouse.getY());
        }
    }*/
	 
	public void NewFruit(GameContainer gc, StateBasedGame sbg) throws SlickException {
		randomPos = ThreadLocalRandom.current().nextInt(1,7);
		shiftX = posdiffx*randomPos;
		shiftY = posdiffy;
		velX = 6;
		velY = 15;
}
	public void shuffle(fruit fruits) throws SlickException {
		randomNum=ThreadLocalRandom.current().nextInt(0,7);
		while(fruits.randomNum==randomNum) {
			randomNum=ThreadLocalRandom.current().nextInt(0,7);		
		}
		fruits.fruitImage=fruits.nextImagee(randomNum);		
	}
	public void motion() {
		if(startover==1) {
			float time = deltaTime * 0.00025f;
			fruits[i+1].shiftX += (velX*time);
			fruits[i+1].shiftY -= (velY*time*2.3);
			velY -= grav*time;
			lives=3;
			startover=0;
		}
		else {
		float time = deltaTime * 0.00025f;
		fruits[i].shiftX += (velX*time);
		//fruits[i].shiftX -= (velX*time);	
		fruits[i].shiftY -= (velY*time*2.3);
		velY -= grav*time;

		if(fruits[i].shiftX > 590) {
			velX = -velX*0.8f;
		}
		if(fruits[i].shiftX < 1) {
			velX = -velX*0.8f;
		}
		if(fruits[i].shiftY < 1) {
			velY = -velY*1.2f;
		}
		}
	}
	
	public void isSliced(GameContainer gc,Input input,fruit fruits) throws SlickException{
		if(input.isMousePressed(0)) {
			fruits.sliced=true;
		if(fruits.imageName == "res/Bomb.png")
		{
			gameoverflag=1;
			bombexplode=new Sound("res/powerup-deflect-explode.wav");
			bombexplode.play();
		}else if(fruits.imageName == "res/Peach.png"){
			score += 3;
		}else{
			score++;
		}
		fruits.fruitImage = fruits.nextImageeSliced(fruits.imageName);
		//fruits.nextImageeSliced2(fruits.imageName);
		fruitcut=new Sound("res/Impact-Apple.wav");
		fruitcut.play();
		//fruit1.fruitImage = fruits.slice1;
		//fruit2.fruitImage = fruits.slice2;
		//fruit1.velX = fruits.velX;
		//fruit2.velX = -fruits.velX;
			}
		}
	
	public void setHighScore(int score) {
	    try {
	    	BestScore Highscore = new BestScore();
	    	Highscore.setClassicscore(score);
	    	JAXBContext jc = JAXBContext.newInstance(BestScore.class);
	    	Marshaller ms = jc.createMarshaller();
	    	ms.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    	ms.marshal(Highscore,(Result) System.in);
	    	ms.marshal(Highscore,new File("save.xml"));
	    }
	    catch(Exception e){
	    	System.out.println(""+e.getMessage());
	    }
		}
	
	public int getID() {
		return 1;
	}
}