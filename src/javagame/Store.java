package javagame;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;


public class Store extends BasicGameState{

	Image Background;
	int[] duration = {200,200};
	float fruitPositionX=0;
	float fruitPositionY=0;
	float shiftX=fruitPositionX+320;//half of the full screen
	float shiftY= fruitPositionY +210;
	
	public Store(int state) {

	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{
		Background = new Image("res/MenuBackground.png");
	}
	
	//Draw
	public void render(GameContainer gc, StateBasedGame sbg,Graphics g) throws SlickException {
		Background.draw(fruitPositionX,fruitPositionY);
		g.drawString("Store!", 100, 100);
	}
	
	//animation movement
	public void update(GameContainer gc, StateBasedGame sbg,int delta) throws SlickException{
		
	}
	
	public int getID() {
		return 3;
	}
}