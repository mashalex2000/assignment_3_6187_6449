package javagame;

import java.util.concurrent.ThreadLocalRandom;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;


public class fruit {
	Boolean sliced = false;
	String imageName = " ";
	int randomPos = ThreadLocalRandom.current().nextInt(1,7);
	float posdiffx = 80, posdiffy = 370;
	float shiftX =posdiffx*randomPos;
	float shiftY =posdiffy;
	float grav = 0.98f, deltaTime = 100;
	double velX =6;
	double velY =15;
	int[] duration = {200,200};
	Animation fruitMovement;	
	Image fruitImage,slice1,slice2;
	Animation fruit;
	int randomNum = ThreadLocalRandom.current().nextInt(0,7);
	
	
	public Vector2f velocity() {
		return new Vector2f((float)velX,-(float)velY);
	} 
	
	public Vector2f resistance() {
		return new Vector2f(0,grav);
	}
	
public Image nextImagee(int u) throws SlickException{
		
		switch(u) {
		case(1):{
			imageName = "res/Apple.png";
			fruitImage = new Image(imageName);
			break;}
		case(2):{
			imageName = "res/Peach.png";
			fruitImage = new Image(imageName);
			break;}
		case(3):{
			imageName = "res/Mango.png";
			fruitImage = new Image(imageName);
			break;}
		case(4):{
			imageName = "res/Bomb.png";
			fruitImage = new Image(imageName);
			break;}
		case(0):{
			imageName = "res/Banana.png";
			fruitImage = new Image(imageName);
			break;}
		case(5):{
			imageName = "res/Strawberry.png";
			fruitImage = new Image(imageName);
			break;}
		case(6):{
			imageName = "res/Watermelon.png";
			fruitImage = new Image(imageName);
			break;}

		}
      	return fruitImage;
	}
	
/*public Animation nextImagee(int u) throws SlickException{
		
		switch(u) {
		case(1):{
			imageName = "res/Apple.png";
			Image[] moveUp1 = {new Image(imageName),new Image(imageName)};
	      	fruitMovement = new Animation(moveUp1,duration,false);
			break;}
		case(2):{
			imageName = "res/Peach.png";
			Image[] moveUp2 = {new Image(imageName),new Image(imageName)};
	      	fruitMovement = new Animation(moveUp2,duration,false);
			break;}
		case(3):{
			imageName = "res/Mango.png";
			Image[] moveUp3 = {new Image(imageName),new Image(imageName)};
	      	fruitMovement = new Animation(moveUp3,duration,false);
			break;}
		case(4):{
			imageName = "res/Bomb.png";
			Image[] moveUp4 = {new Image(imageName),new Image(imageName)};
	      	fruitMovement = new Animation(moveUp4,duration,false);
			break;}
		case(0):{
			imageName = "res/Banana.png";
			Image[] moveUp0 = {new Image(imageName),new Image(imageName)};
	      	fruitMovement = new Animation(moveUp0,duration,false);
			break;}
		case(5):{
			imageName = "res/Strawberry.png";
			Image[] moveUp5 = {new Image(imageName),new Image(imageName)};
	      	fruitMovement = new Animation(moveUp5,duration,false);
			break;}
		case(6):{
		imageName = "res/Watermelon.png";
		Image[] moveUp6 = {new Image(imageName),new Image(imageName)};
      	fruitMovement = new Animation(moveUp6,duration,false);
		break;}

		}
      	return fruitMovement;
	}*/

public Image nextImageeSliced(String imageName) throws SlickException{
	
	switch(imageName) {
	case("res/Apple.png"):{
		fruitImage = new Image("res/Appleslice.png");	
      	break;}
	case("res/Peach.png"):{
		fruitImage = new Image("res/Peach.png");	
      	break;}
	case("res/Mango.png"):{
		fruitImage = new Image("res/Mangoslice.png");
		break;}
	case("res/Bomb.png"):{
		fruitImage = new Image("res/Bomb.png");
		break;}
	case("res/Banana.png"):{
		fruitImage = new Image("res/Bananaslice.png");
		break;}
	case("res/Strawberry.png"):{
		fruitImage = new Image("res/Strawberryslice.png");
		break;}
	case("res/Watermelon.png"):{
		fruitImage = new Image("res/Watermelonslice.png");
      	break;}

	}
  	return fruitImage;
}

/*public Image nextImageeSliced2(String imageName) throws SlickException{
	
	switch(imageName) {
	case("res/Apple.png"):{
		slice2 = new Image("res/Appleslice2.png");
		break;}
	case("res/Peach.png"):{
		slice2 = new Image("res/Peach.png");
		break;}
	case("res/Mango.png"):{
		slice2 = new Image("res/Mangoslice2.png");
		break;}
	case("res/Bomb.png"):{
		slice2 = new Image("res/Bomb.png");
		break;}
	case("res/Banana.png"):{
		slice2 = new Image("res/Bananaslice2.png");
		break;}
	case("res/Strawberry.png"):{
		slice2 = new Image("res/Strawberryslice2.png");
		break;}
	case("res/Watermelon.png"):{
		slice2 = new Image("res/Watermelonslice2.png");
	break;}

	}
  	return slice2;
}*/
	
}